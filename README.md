UpDimV3
==================
The repository contain example codes to train the UpDim model

This project require the following libraries:
* torch
* [torchelie](https://github.com/Vermeille/Torchelie) 
* scipy
* soundfile
* numpy as np
* [tqdm](https://github.com/tqdm/tqdm)

The files in this project are
* sam.py contains the code for the Sharpness aware minimizer (SAM) 
* UpDimV3.py contains the UpDimV3 class
* UpDimV3_quebec.py contains the codes to train the model with Adam optimizer
* UpDimV3_quebec_SAM.py contains the codes to train the model with SAM optimizer

Usage
-----
For the main scripts, the options can be displayed with `python script_name -h`

You can train a network with
```shell
python UpDimV3_quebec_SAM.py --train xeno_train_1500.npy mcaulay_train_2200.npy
```

The train and test files should be array containing the path to the sound files that will be used.
The abiotic file `sons_abiotiques.npy` is also requiered.
The database `xeno_train_1500.npy` and`mcaulay_train_2200.npy` are private.