import torch
import torchelie as tch
import torchelie.callbacks.callbacks as tcb
import scipy.signal as sg
import soundfile as sf
import argparse
import numpy as np
from tqdm import tqdm, trange
from sam import SAMSGD
import torchelie.utils as tu
from UpDimV3 import UpDimV3

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description="""Train an UpDimV3 model with SAM optimizer""")
parser.add_argument("--train",
                    default=['xeno_train_1500.npy'],
                    type=str, nargs='*', help="Paths to train file(s)")
parser.add_argument("--test",
                    default=['JF_propres_43.npy'],
                    type=str, nargs='*', help="Paths to test file(s)")
parser.add_argument("--rng", action='store_false', help='unset rng state')
parser.add_argument("--weight", type=str, default='', help="recipy weight for resume")
parser.add_argument("--from_ckpt", type=str, default='', help="Model weight for resume")
parser.add_argument("--hot_weight", type=str, default='', help="Model weight for hot restart")
parser.add_argument("--run_name", type=str, default='', help='suffix of the run name')

args = parser.parse_args()


batch_size = 32
sound_len = 7
sound_sr = 22050
num_feature = sound_len * sound_sr
num_classes = 43

if args.rng:
    rng = np.random.RandomState(42)
else:
    rng = np.random.RandomState()


def pink_noise(size, rng, ncols=16, axis=-1):
    """Generates pink noise using the Voss-McCartney algorithm.

    size: either a tuple of int or an int. If an int : number of sample to generate. If a tuple: shape of the return array.
    ncols: number of random sources to add. Should be high enough so that num_samples*0.5**(ncols-2) is near zero.
    axis: axis which contains the sound samples. Generate white noise otherwise.

    returns: NumPy array of shape size
    """
    if type(size) is not tuple:
        size = (size,)
    array = rng.rand(*size)
    assert -len(size) <= axis < len(size)
    axis %= len(size)
    axis += 1
    # the total number of changes is nrows
    cols = rng.geometric(0.5, size)
    cols[cols >= ncols] = 0
    cols = (1. * (np.arange(1, ncols).reshape((-1,) + len(size) * (1,)) == cols)).swapaxes(axis, -1)
    cols[..., 0] = 1.
    cols = np.cumsum(cols).reshape(cols.shape).astype(int).swapaxes(axis, -1)
    array = np.concatenate([array[np.newaxis], rng.rand(cols.max() + 1)[cols]], axis=0).sum(0)
    return array


class Dataset:
    def __init__(self, paths, train, rank, rng=None):
        self.path = np.concatenate([np.load(p) for p in paths])
        self.wav = len(self.path) * [None]
        self.labels = len(self.path) * [None]
        for i, p in enumerate(tqdm(self.path)):
            self.wav[i] = p
            self.labels[i] = p.split('/')[-1][:4]
        self.wav = np.array([x for x in self.wav if x is not None])
        self.labels = np.array([x for x in self.labels if x is not None])
        self.names, self.labels = np.unique(self.labels, return_inverse=True)
        assert len(self.wav) == len(self.labels)
        print(f'Found {len(self.wav)} wav')
        assert len(self.names) == num_classes
        self.labels = np.eye(num_classes)[self.labels]
        self.classes = self.names
        self.train = train
        self.rank = rank
        self.rng = rng
        self.wind = np.load('/nfs/NAS4/anatole/data/quebec/sons_abiotiques/sons_abiotiques.npy').squeeze()
        self.test_loaded = False
        if not self.train:
            print(f'Loading test dataset')
            self.test_tuples = self.__len__() * [(None, None)]
            for i in trange(self.__len__()):
                self.test_tuples[i] = self.__getitem__(i)
            self.test_loaded = True

    def __len__(self):
        return len(self.wav)

    def get_class_names(self):
        return self.names

    def __getitem__(self, i):
        if self.test_loaded and not self.train:
            return self.test_tuples[i]
        p = self.wav[i]
        label = self.labels[i]
        sample, sr = sf.read(p, always_2d=True)
        sample = sample[:, 0]
        if sr != sound_sr:
            sample = sg.resample(sample, int(len(sample)*sound_sr/sr))
        if self.train:
            sample = sg.resample(sample, int(np.clip(len(sample) * rng.normal(1, 0.025, 1),
                                                     num_feature+10, 1.10*len(sample))))
            p = self.rng.randint(0, len(sample) - num_feature)
            r = self.rng.randint(0, 100)
            if r < 20:
                rev = np.zeros(sound_sr)
                if r % 3:
                    rev += self.rng.normal(0, 0.001, len(rev))
                rev[0] = 1
                for k in range(r%4):
                    rev[self.rng.randint(0.001*sound_sr, len(rev))] = self.rng.uniform(0, 1)
                sample = torch.nn.functional.conv1d(torch.Tensor(sample[None, None]).to(self.rank),
                                                    torch.Tensor(np.flip(rev).copy()[None, None]).to(self.rank),
                                                    padding=len(rev)//2+1).cpu().numpy().squeeze()
            sample = sample[p:p+num_feature]
        else:
            sample = sample[:num_feature]
        sample -= sample.mean(-1,keepdims=True)
        sample /= sample.std(-1,keepdims=True) + 1e-18

        if self.train:
            sample += self.rng.normal(0, 1, num_feature) * 10**self.rng.uniform(-2.5, -0.12, 1)
            sample += (lambda x: x-x.mean(-1,keepdims=True)/x.std(-1,keepdims=True))\
                          (np.cumsum(self.rng.normal(0, 1, num_feature))) * 10**self.rng.uniform(-2.5, -0.5, 1)
            sample += ((lambda x: x-x.mean(-1,keepdims=True)/x.std(-1,keepdims=True))
                (pink_noise(num_feature, self.rng))) * 10**self.rng.uniform(-4, -1, 1)
            wind, sr_wind = sf.read(self.rng.choice(self.wind), always_2d=True)
            wind = wind[:, 0]
            if sr != sound_sr:
                wind = sg.resample(wind, int(len(wind) * sound_sr / sr))
            wind = sg.resample(wind, int(np.clip(len(wind) * rng.normal(1, 0.025, 1),
                                                 num_feature + 10, 1.10 * len(wind))))
            p_wind = self.rng.randint(0, len(wind) - num_feature)
            sample += (lambda x: x-x.mean(-1,keepdims=True)/x.std(-1,keepdims=True))(wind[p_wind:p_wind + num_feature])\
                      * 10**self.rng.uniform(-2, -0.05, 1) * (-1)**self.rng.randint(0, 10)
            sample -= sample.mean(-1, keepdims=True)
            sample /= sample.std(-1, keepdims=True)
            p = self.rng.randint(0, len(sample) - int(sound_sr * 0.5))
            sample[p:p+self.rng.randint(0, sound_sr * 0.5)] = 0
            sample *= (-1)**self.rng.randint(0, 10)

        return torch.from_numpy(sample[np.newaxis]).float(), np.argmax(label)

def train(rank, world_size):
    ds = torch.utils.data.DataLoader(tch.datasets.MixUpDataset(Dataset(args.train, True, rank, rng), 0.3),
                                     batch_size//world_size,
                                     shuffle=True,
                                     drop_last=True,
                                     num_workers=6,
                                     pin_memory=True)
    test_dst = Dataset(args.test, False, rank)
    dst = torch.utils.data.DataLoader(test_dst,
                                      batch_size=batch_size//world_size,
                                      num_workers=8,
                                      shuffle=True)

    print('Dataset loaded')

    model = UpDimV3(num_classes)
    if args.from_ckpt != '':
        model.load_state_dict(
            torch.load(args.from_ckpt, map_location='cuda:' +
            str(rank))['model'])
    model = torch.nn.parallel.DistributedDataParallel(model.to(rank), [rank], rank)

    def accuracy(preds, labels):
        with torch.no_grad():
            id_preds = torch.max(preds, 1)[1]
            id_labels = torch.max(labels, 1)[1]
            return 100 * (id_preds == id_labels).sum().item() / len(labels)

    opt = SAMSGD(model.parameters(),
                             lr=5e-3,
                             weight_decay=0.005,
                             momentum=0.9)                         

    def train(batch):
        x, y = batch
        def closure():
          opt.zero_grad()
          pred = model(x)
          loss = tch.loss.continuous_cross_entropy(pred, y)
          loss.backward()
          return loss, pred
        
        #norm = torch.nn.utils.clip_grad_norm_((p for pg in opt.param_groups for p in pg['params']), 5)
        loss, pred = opt.step(closure)
        return {'loss': loss, 'pred': pred, 'accuracy': accuracy(pred, y),  'grad_norm':0}

    def test(batch):
        x, y = batch
        pred = model(x)
        y = torch.eye(num_classes)[y].to(y.device)
        loss = tch.loss.continuous_cross_entropy(pred, y)
        return {'loss': loss, 'pred': pred, 'accuracy': accuracy(pred, y)}

    recipe = tch.recipes.TrainAndTest(model,
                                      train,
                                      test,
                                      ds,
                                      dst,
                                      test_every=1000,
                                      log_every=64,
                                      checkpoint='model3sam_quebec' + args.run_name if rank == 0 else None,
                                      visdom_env=None,
                                      key_best=(lambda x: x['test_loop']['callbacks']['state']['metrics']['accuracy']))
                                      
    class OptimizerLog(tu.AutoStateDict):
  
      def __init__(self,
                   opt,
                   clip_grad_norm=None,
                   log_lr=False,
                   log_mom=False):
          super(OptimizerLog, self).__init__()
          self.opt = opt
          self.log_lr = log_lr
          self.log_mom = log_mom
          self.clip_grad_norm = clip_grad_norm
  
      def on_batch_start(self, state):
          if self.log_lr:
              for i in range(len(self.opt.param_groups)):
                  pg = self.opt.param_groups[i]
                  state['metrics']['lr_' + str(i)] = pg['lr']
  
          if self.log_mom:
              for i in range(len(self.opt.param_groups)):
                  pg = self.opt.param_groups[i]
                  if 'momentum' in pg:
                      state['metrics']['mom_' + str(i)] = pg['momentum']
                  elif 'betas' in pg:
                      state['metrics']['mom_' + str(i)] = pg['betas'][0]

    recipe.callbacks.add_callbacks([
        OptimizerLog(opt, log_lr=True, clip_grad_norm=5),
        tcb.LRSched(torch.optim.lr_scheduler.MultiStepLR(opt, list(range(10,80)), 0.95), metric=None),
        tcb.WindowedMetricAvg('loss'),
        tcb.WindowedMetricAvg('accuracy'),
        tcb.WindowedMetricAvg('grad_norm'),
    ])
    recipe.callbacks.add_epilogues([
        tcb.TensorboardLogger(log_dir='UpDimV3SAM_quebec' + args.run_name + '/train' if rank == 0 else None, log_every=64),])
    recipe.test_loop.callbacks.add_callbacks([
        tcb.EpochMetricAvg('loss', False),
        tcb.EpochMetricAvg('accuracy', False),
        # tcb.ConfusionMatrix(test_dst.get_class_names(), True)
    ])
    recipe.test_loop.callbacks.add_epilogues([
        tcb.TensorboardLogger(log_dir='UpDimV3SAM_quebec' + args.run_name + '/test' if rank == 0 else None,
                              log_every=-1),])
    recipe.to(rank)

    if args.hot_weight != '':
        ckpt = torch.load(args.hot_weight)
        del ckpt['callbacks']['callbacks']['Counter_prologue_0']
        del ckpt['callbacks']['callbacks']['LRSched_middle_0']
        del ckpt['test_loop']
        del ckpt['callbacks']['state']['metrics']['lr_0']
        ckpt['callbacks']['callbacks']['Optimizer_middle_0']['opt']['param_groups'][0]['lr'] = opt.param_groups[0]['lr']
        ckpt['callbacks']['callbacks']['Optimizer_middle_0']['opt']['param_groups'][0]['eps'] = opt.param_groups[0]['eps']
        ckpt['callbacks']['callbacks']['Optimizer_middle_0']['opt']['param_groups'][0]['weight_decay'] = opt.param_groups[0]['weight_decay']
        ckpt['callbacks']['callbacks']['Optimizer_middle_0']['opt']['param_groups'][0]['betas'] = opt.param_groups[0]['betas']
        recipe.load_state_dict(ckpt)

    if args.weight != '':
        ckpt = torch.load(args.weight)
        recipe.load_state_dict(ckpt)

    recipe.run(80)

if __name__ == '__main__':
    tch.utils.parallel_run(train)
